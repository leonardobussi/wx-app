import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AmigosDoCovidaoPageRoutingModule } from './amigos-do-covidao-routing.module';

import { AmigosDoCovidaoPage } from './amigos-do-covidao.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AmigosDoCovidaoPageRoutingModule
  ],
  declarations: [AmigosDoCovidaoPage]
})
export class AmigosDoCovidaoPageModule {}
