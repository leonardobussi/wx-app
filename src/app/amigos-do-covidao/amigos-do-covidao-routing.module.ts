import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AmigosDoCovidaoPage } from './amigos-do-covidao.page';

const routes: Routes = [
  {
    path: '',
    component: AmigosDoCovidaoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AmigosDoCovidaoPageRoutingModule {}
