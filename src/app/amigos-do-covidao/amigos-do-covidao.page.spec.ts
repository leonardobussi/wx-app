import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AmigosDoCovidaoPage } from './amigos-do-covidao.page';

describe('AmigosDoCovidaoPage', () => {
  let component: AmigosDoCovidaoPage;
  let fixture: ComponentFixture<AmigosDoCovidaoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmigosDoCovidaoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AmigosDoCovidaoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
